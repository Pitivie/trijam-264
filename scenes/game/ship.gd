extends CharacterBody2D

const SHIFTING = 350.0
const INITIAL_SPEED = 100.0
const accelerate = 50

var init_position
var speed = 0

signal colided

func _ready():
	init_position = position

func reset():
	speed = INITIAL_SPEED
	position = init_position

func _physics_process(delta):
	var direction = Input.get_axis("ui_up", "ui_down")

	if direction:
		velocity.y = direction * SHIFTING
	else:
		velocity.y = move_toward(velocity.y, 0, 100)
	velocity.x = speed

	position.y = clamp(position.y, 130, 350)

	move_and_slide()

func _on_captor_body_shape_entered(body_rid, body, body_shape_index, local_shape_index):
	emit_signal("colided", body_rid)

func speedUp():
	speed += accelerate
	$Bonus.play()

func reInitSpeed():
	speed = max(INITIAL_SPEED, speed * 0.75)
	$Malus.play()
