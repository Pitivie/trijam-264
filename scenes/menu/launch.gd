extends CanvasLayer

signal played

func _on_play_pressed():
	emit_signal("played")
