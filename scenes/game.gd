extends Node2D

@onready var grid = $Grid
@onready var ship = $Ship

signal dead
signal won

var bonus_set = []
var malus_set = []

func _ready():
	bonus_set = grid.get_used_cells_by_id(0, 1)
	malus_set = grid.get_used_cells_by_id(0, 2)

func reset():
	ship.reset()
	for coords in bonus_set:
		grid.set_cell(0, coords, 1, Vector2i(0, 0))
	for coords in malus_set:
		grid.set_cell(0, coords, 2, Vector2i(0, 0))

func _on_ship_colided(body_rid):
	var coords = grid.get_coords_for_body_rid(body_rid)
	var source = grid.get_cell_source_id(0, coords)

	if source == 0:
		emit_signal("dead")
	if source == 1:
		grid.set_cell(0, coords)
		ship.speedUp()
	if source == 2:
		grid.set_cell(0, coords)
		ship.reInitSpeed()

func _on_victory_zone_body_entered(body):
	emit_signal("won")
