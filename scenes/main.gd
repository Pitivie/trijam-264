extends Node2D

@onready var defeat = $Defeat
@onready var victory = $Victory
@onready var game = $Game
@onready var launch = $Launch

func _on_game_dead():
	defeat.visible = true

func _on_game_won():
	victory.visible = true

func _on_launch_played():
	launch.visible = false
	game.reset()

func _on_victory_again():
	victory.visible = false
	launch.visible = true

func _on_defeat_again():
	defeat.visible = false
	launch.visible = true
